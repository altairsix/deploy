package main

import (
	"context"
	"encoding/json"
	"io"
	"io/ioutil"
	"log"
	"os"

	"bitbucket.org/altairsix/deploy/internal/amazonecs"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ecs"
	"gopkg.in/urfave/cli.v1"
)

type Options struct {
	Region string
	Input  string
}

var opts Options

const (
	Version = "0.1.0"
)

func main() {
	app := cli.NewApp()
	app.Version = Version
	app.Usage = "utility to simplify deployment process"
	app.Commands = cli.Commands{
		{
			Name:  "ecs",
			Usage: "ecs tasks",
			Subcommands: cli.Commands{
				{
					Name:   "register-task-definition",
					Usage:  "register ecs task",
					Action: registerTask,
					Flags: []cli.Flag{
						cli.StringFlag{
							Name:        "region",
							Value:       "us-west-2",
							Usage:       "amazon default region",
							EnvVar:      "AWS_DEFAULT_REGION",
							Destination: &opts.Region,
						},
						cli.StringFlag{
							Name:        "input",
							Usage:       "json configuration file",
							Destination: &opts.Input,
						},
					},
				},
				{
					Name:        "update-service",
					Usage:       "update ecs service",
					Description: "updates an ecs service. if the service does not exist, then the service will be created",
					Action:      updateService,
					Flags: []cli.Flag{
						cli.StringFlag{
							Name:        "region",
							Value:       "us-west-2",
							Usage:       "amazon default region",
							EnvVar:      "AWS_DEFAULT_REGION",
							Destination: &opts.Region,
						},
						cli.StringFlag{
							Name:        "input",
							Usage:       "json configuration file",
							Destination: &opts.Input,
						},
					},
				},
			},
		},
	}
	app.Run(os.Args)
}

func check(err error) {
	if err != nil {
		log.Fatalln(err)
	}
}

func registerTask(_ *cli.Context) error {
	cfg := &aws.Config{Region: aws.String(opts.Region)}
	s, err := session.NewSession(cfg)
	check(err)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	data, err := ioutil.ReadFile(opts.Input)
	check(err)

	input := &ecs.RegisterTaskDefinitionInput{}
	err = json.Unmarshal(data, input)
	check(err)

	ecsAPI := ecs.New(s)
	version, err := amazonecs.RegisterTask(ctx, ecsAPI, input)
	check(err)

	io.WriteString(os.Stdout, version)
	os.Stdout.Sync()

	return nil
}

func updateService(_ *cli.Context) error {
	cfg := &aws.Config{Region: aws.String(opts.Region)}
	s, err := session.NewSession(cfg)
	check(err)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	data, err := ioutil.ReadFile(opts.Input)
	check(err)

	input := &ecs.UpdateServiceInput{}
	err = json.Unmarshal(data, input)
	check(err)

	ecsAPI := ecs.New(s)
	out, err := amazonecs.UpdateService(ctx, ecsAPI, input)
	check(err)

	encoder := json.NewEncoder(os.Stdout)
	encoder.SetIndent("", "  ")
	encoder.Encode(out)

	return nil
}
