package amazonecs

import (
	"context"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/ecs"
	"github.com/pkg/errors"
)

func newCreateServiceInput(input *ecs.UpdateServiceInput) *ecs.CreateServiceInput {
	return &ecs.CreateServiceInput{
		Cluster:                 input.Cluster,
		DeploymentConfiguration: input.DeploymentConfiguration,
		DesiredCount:            input.DesiredCount,
		PlacementStrategy: []*ecs.PlacementStrategy{
			{
				Field: aws.String("attribute:ecs.availability-zone"),
				Type:  aws.String(ecs.PlacementStrategyTypeSpread),
			},
			{
				Field: aws.String("instanceId"),
				Type:  aws.String(ecs.PlacementStrategyTypeSpread),
			},
		},
		Role:           nil,
		ServiceName:    input.Service,
		TaskDefinition: input.TaskDefinition,
	}
}

func UpdateService(ctx context.Context, ecsAPI *ecs.ECS, input *ecs.UpdateServiceInput) (interface{}, error) {
	out, err := ecsAPI.UpdateServiceWithContext(ctx, input)
	if err != nil {
		if v, ok := err.(awserr.Error); ok {
			switch v.Code() {
			case ecs.ErrCodeServiceNotFoundException:
				createIn := newCreateServiceInput(input)
				out, err := ecsAPI.CreateServiceWithContext(ctx, createIn)
				if err != nil {
					return nil, errors.Wrapf(err, "unable to create service, %v", createIn.ServiceName)
				}
				return out, nil
			}
		}

		return nil, errors.Wrapf(err, "unable to register task")
	}

	return out, nil
}
