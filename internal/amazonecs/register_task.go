package amazonecs

import (
	"context"
	"strconv"

	"github.com/aws/aws-sdk-go/service/ecs"
	"github.com/pkg/errors"
)

func RegisterTask(ctx context.Context, ecsAPI *ecs.ECS, input *ecs.RegisterTaskDefinitionInput) (string, error) {
	out, err := ecsAPI.RegisterTaskDefinitionWithContext(ctx, input)
	if err != nil {
		return "", errors.Wrapf(err, "unable to register task")
	}

	return strconv.FormatInt(*out.TaskDefinition.Revision, 10), nil
}
