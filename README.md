deploy
------

deploy is a cli tool to simplify deployments to aws ecs.

### deploy ecs register-task

registers a new aws ecs task, returns ```service:revision```

### deploy ecs update-service

updates the specified service; if the service doesn't exist, it will be created
